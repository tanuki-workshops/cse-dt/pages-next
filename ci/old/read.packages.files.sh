# this section is just for your information
# you can delete it
# packages list
#curl --header "JOB-TOKEN: $CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages" > package.list.json
#cat package.list.json | jq -r '.[]|[.id, .name, .version] | @tsv' |
#while IFS=$'\t' read -r id name version; do
#  echo "📦 ${id} ${name} ${version}" # saved 0.0.0 package
#  if [ "$name" = "saved" ]
#  then
#    # files of the package
#    curl --header "JOB-TOKEN: $CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/${id}/package_files" > package_files.list.json
#    cat package_files.list.json | jq -r '.[]|[.id, .package_id, .file_name] | @tsv' |
#    while IFS=$'\t' read -r id package_id file_name; do
#      echo "📝 ${id} ${package_id} ${file_name}" # 📝 59133753 10537843 blue-web-app.tar.gz
#      if [[ $file_name = "$CI_COMMIT_REF_NAME.tar.gz" ]]
#      then
#        echo "🗑️ $file_name should be deleted at the merge"
#      fi
#    done
#  fi
#done
