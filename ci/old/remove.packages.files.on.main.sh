# ---------------------------------------------------------
# Remove all main.tar.gz (if we are on the main branch)
# ---------------------------------------------------------
if [ "$CI_COMMIT_BRANCH" = "main" ]
then
    echo "👋 removing the main.tar.gz"
    # packages list
    curl --header "JOB-TOKEN: $CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages" > package.list.json
    cat package.list.json | jq -r '.[]|[.id, .name, .version] | @tsv' |
    while IFS=$'\t' read -r id name version; do
    echo "📦 ${id} ${name} ${version}" # saved 0.0.0 package
    if [ "$name" = "saved" ]
    then
        # files of the package
        curl --header "JOB-TOKEN: $CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/${id}/package_files" > package_files.list.json
        cat package_files.list.json | jq -r '.[]|[.id, .package_id, .file_name] | @tsv' |
        while IFS=$'\t' read -r id package_id file_name; do
        echo "📝 ${id} ${package_id} ${file_name}" # 📝 59133753 10537843 blue-web-app.tar.gz
        if [ "$file_name" = "$CI_COMMIT_BRANCH.tar.gz" ]
        then
            echo "🗑️ deleting $file_name"
            curl --request DELETE --header "PRIVATE-TOKEN: $BOT_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/${package_id}/package_files/${id}"
        fi
        done
    fi
    done
fi
