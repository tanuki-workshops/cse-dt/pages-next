# web-app-pages 

## Settings
The source code of the application is located at `./website`.
> if you change this, change the value of `WEBSITE_LOCATION` in `.gitlab-ci.yml`

```yaml
variables:
  WEBSITE_LOCATION: "website"
```

- The project visibility must be public
- The Pages must be available for everyone

## Delete files in the generic package (`save` `0.0.0`)
> project access token is an enterprise feature

Every merge request is saving the files of the web application to a file `${CI_COMMIT_REF_NAME}.tar.gz` and stores it to the generic package registry in the `saved` package (the tag is always `0.0.0`).
At every merge on the `main` branch, to be allowed to delete the unused `${CI_COMMIT_REF_NAME}.tar.gz`, you need to:

- Create a **project access token** (with all rights on the API + Maintainer or Owner role)
- Copy the value of the token in a CI/CD variable named `BOT_TOKEN`

